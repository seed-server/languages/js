<?xml version = "1.0" encoding = "UTF-8" standalone = "no" ?>


<?php
include($_SERVER["DOCUMENT_ROOT"]."/seed-env/plugins/php/settings.php");

if( array_key_exists("path",$_GET) ){
$input_path=$_GET["path"];
}else{
$input_path=".";
}

//-------------------------------------------------------------
function urlX($path) {
    global  $MAIN_PATH;
    global  $MAIN_URL;
    $string=str_replace($MAIN_PATH,$MAIN_URL, $path);
    return $string;
}
//-------------------------------------------------------------
function make($dirs){

new InitAll($dirs,["event"=>"init"]);
$dirs->setup();
//tree($dirs);
?>
        <Bd_Data name="<?=$dirs->get("name")?>" text="<?=url($dirs)?>"  basename="<?=$dirs->get("basename")?>" 
                url="<?=url($dirs)?>" 
                type="dir">
<?php

foreach($dirs->search('*/[DiscNode]/[type=dir]')->iter() as $elt){

        make($elt);

}

foreach($dirs->search('*/[DiscNode]/[type=file]')->iter() as $elt){
        $basename= $elt->get("basename");
        $name=$elt->get("name");
        $ext=$elt->get("ext");
?>
        <Bd_Data name="<?=$name?>" text="<?=url($elt)?>" 
                url="<?=url($elt)?>"  ext="<?=$ext?>" basename="<?=$basename?>" 
                type="file"/>
<?php

}
?>
        </Bd_Data>
<?php
}
$dirs= new DiscNode(null,["path"=>$input_path]);
make($dirs);
?>

