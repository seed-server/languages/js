
        
//========================================================

class Query {

//========================================================
    /*
    object to handle query
    with an optional output format option.

    list of formats :
    * json
    * xml
    * obj -> creer les objets js
    
    basic usage returns text:
        var q=new Query(url,callback);

    usage:
        var q=new Query(url,callback,format);

    cyclic behavior, dt in millis:
        var q=new Query(url,callback,format,dt);

    callback function :
        function callback(data){
            ...
        }
    */

    //----------------------------------------------------
    constructor(obj,url,callback,format=null,dt=null){
    //----------------------------------------------------


        //attributes
        this.obj=obj;
        this.component_cb=callback;
        this.url=url;
        this.dt=dt;

        //request
        this.request = new XMLHttpRequest();
        this.data;
        this.interval;
        this.format=format;

        //query
        if ( dt ) {
            this.cycle_start(dt);

        }else{
            this.query();
        }

    }

    //----------------------------------------------------

    cycle_start(dt) {

    //----------------------------------------------------

          this.interval = setInterval(this.query.bind(this), this.dt);

    }
    //----------------------------------------------------

    cycle_stop() {

    //----------------------------------------------------

        clearInterval(this.interval);

    }
    //----------------------------------------------------

    query() {

    //----------------------------------------------------

          this.request.open('GET', this.url, true);
          this.request.onreadystatechange = this.request_cb.bind(this);
          this.request.send(null);

    }
    //----------------------------------------------------

    request_cb(e) {

    //----------------------------------------------------

        if (this.request.readyState === 4) {
            if (this.request.status === 200) {

                this.data = this.request.responseText;
                console.log(this.data);

                if(this.format=="json"){
                    this.data=JSON.parse (this.data);
                }
                if(this.format=="xml"){
                      this.data=decodeURI(this.data);
                      var parser = new DOMParser();

                      var xmlDoc = parser.parseFromString(this.data,"text/xml");
                      this.data=xmlDoc.documentElement;
                }
                if(this.format=="obj"){
                      this.data=this.data.replace("&","&amp;");
                      var parser = new DOMParser();

                      var xmlDoc = parser.parseFromString(this.data,"text/xml");
                      this.data=from_xml(null,xmlDoc.documentElement);
                }

                //console.log("loading",this.url,"done");
                this.component_cb.bind(this.obj)(this.data);
                return

            }
        console.warn("error",this.url);
        }

    }

    //----------------------------------------------------


}
//========================================================




