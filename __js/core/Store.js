//========================================================

class Store {

//========================================================
/*
    crée une arborescence parent/enfants pour gerer les objets
    utiliser par héritage pour creer différents objets
    imbricables
*/

    //----------------------------------------------------
    constructor(lst=null){
    //----------------------------------------------------
        if (lst){
            this.elements=lst;
        }else{
            this.elements=[];
        }

        this.STORE=Store;

    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------
        this.execute("destroy");
        this.elements=[];
    }
    //----------------------------------------------------
    destroy_class(cls){
    //----------------------------------------------------
        this.by_class(cls).destroy();

    }

    //----------------------------------------------------
    empty(){
    //----------------------------------------------------
        return (this.elements.length == 0);
    }
    //----------------------------------------------------
    size(){
    //----------------------------------------------------
        return this.elements.length;
    }

    //----------------------------------------------------
    first(){
    //----------------------------------------------------
        if (this.empty() ){
            return null;
        }else{
            return this.elements[0];
        }
    }
    //----------------------------------------------------
    iter(){
    //----------------------------------------------------
        return this.elements;
    }
    //----------------------------------------------------
    append(elt){
    //----------------------------------------------------
        //console.log(this.elements,elt);
        if( elt instanceof Store){

            this.elements=this.elements.concat(elt.elements); 
        }else if(Array.isArray(elt)){
            this.elements=this.elements.concat(elt); 
        }else{

            this.elements.push(elt);
        }

    }
    //----------------------------------------------------
    remove(elt){
    //----------------------------------------------------
        var result=new this.STORE();
        for(var i in this.elements) {
            if (this.elements[i]!=elt){
                result.append(this.elements[i]);  
            }
        }
        this.elements=result;

    }
    //----------------------------------------------------
    by_class(cls){
    //----------------------------------------------------
        var result=new this.STORE();

        for(var i in this.elements) {
            if( this.elements[i] instanceof cls ){
                result.append(this.elements[i]); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    by_attr(attr){
    //----------------------------------------------------
        var result=new this.STORE();

        for(var i in this.elements) {
            if( attr in this.elements[i] ){
                result.append(this.elements[i]); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    by_attr_value(attr,value){
    //----------------------------------------------------
        var result=new this.STORE();

        for(var i in this.elements) {
            if( attr in this.elements[i] ){
                if( this.elements[i][attr]==value ){
                    result.append(this.elements[i]); 
                }
            }
        }

        return result;

    }

    //----------------------------------------------------
    execute(action,...args){
    //----------------------------------------------------

        var result=new this.STORE();
        var lst=this.by_attr(action).iter();

        for(var i in lst) {
            //console.log(action,lst[i].path(),args);
            var r=lst[i][action](...args);
            result.append([lst[i],r]); 

        }

        return result;

    }

    //----------------------------------------------------
    sorted(){
    //----------------------------------------------------
        var elements = this.iter();
        var sortedArray = [];
        var result = [];

        // Push each JSON Object entry in array by [key, value]
        for(var i in elements)
        {
            sortedArray.push([elements[i].name, elements[i]]);
        }

        // Run native sort function and returns sorted array.
        sortedArray=sortedArray.sort();

        for(var i in sortedArray)
        {
            result.push( sortedArray[i][1]);
        }
        return result;

    }
    //----------------------------------------------------

}
//========================================================
