

//========================================================

class Curve extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("format");
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

                //console.log(selected.text);
        if (this.format=="json"){
            var items=JSON.parse (selected.text);

        }else if (this.format=="csv"){
            var items=[];
            var value;
            var lst=selected.text.split(";");
            for (var i in lst){
                if(lst[i]!=""){
                    value=lst[i].split(",");
                    //console.log(value);
                    items.push({ x: parseFloat(value[0]), y:parseFloat(value[1]) });
                }
            }
        }

        var dataset = new vis.DataSet(items);
        var options = {
        sort: false,
        sampling:false
        };

//        style:'points'
        this.graph2d = new vis.Graph2d(div[0], dataset, options);

    }
    //----------------------------------------------------

}
//========================================================




