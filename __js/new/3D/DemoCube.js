  import {Object3D} from './Object3D.js';
  import * as THREE from '/seed-env/__static/threejs/three.module.js';
//======================================================
export class DemoCube extends Object3D {
//======================================================


    //---------------------------------------------------
    onBuild3d() {
    //---------------------------------------------------
        // objets de la scene
        this.geometry= new THREE.BoxGeometry( 0.2, 0.2, 0.2 );
        this.material = new THREE.MeshNormalMaterial();
        return new THREE.Mesh( this.geometry, this.material );

  }
    //---------------------------------------------------
    onRun(time) {
    //---------------------------------------------------

        this.object3d.rotation.x = time / 2000;
        this.object3d.rotation.y = time / 1000;

    }
    //---------------------------------------------------
}
//======================================================
