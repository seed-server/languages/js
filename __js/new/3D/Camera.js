  import {Object3D} from './Object3D.js';
  import * as THREE from '/seed-env/__static/threejs/three.module.js';
//======================================================
export class Camera extends Object3D {
//======================================================


    //---------------------------------------------------
    onBuild3d() {
    //---------------------------------------------------
        var camera = new THREE.PerspectiveCamera( 70, this.parent.width /  this.parent.height, 0.01, parseInt(this.data.r) );

        camera.position.z = 1;
        return camera;
  }
    //---------------------------------------------------
    onRun(time) {
    //---------------------------------------------------
        this.parent.camera=this.object3d;


    }
    //---------------------------------------------------
}
//======================================================
