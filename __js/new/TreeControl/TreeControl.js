import {TreeNode} from './../Tree/TreeNode.js';
import {HtmlElement} from './../Html/HtmlElement.js';




function get_node(name,node){
    var data={};
    data["text"]=name;
    data["select"]=node.path();
    return data;
}




//==============================================
export class TreeControlItem extends HtmlElement {
//==============================================

    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<div>");

        return node;
      }

 
    //---------------------------------------------------
    modify(selected) {
    //---------------------------------------------------
        this.parent.modify(selected);
      }
    //--------------------------------------------

}
//==============================================
export class MainAreaButton extends TreeControlItem {
//==============================================
    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<div>")
                    .text(this.data.text)
                    .addClass("MainAreaButton")
                    .click(this.onclick.bind(this));
        return node;
      }



    //---------------------------------------------------
    onclick() {
    //---------------------------------------------------
        this.parent.modify(this.data.select);
      }
    //--------------------------------------------

}
//==============================================
export class MainArea extends TreeControlItem {
//==============================================


    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<div>")
                    .addClass(this.data.classes);
        this.clear();
        for (let elt in this.data.dataset){
            new MainAreaButton(this,this.data.dataset[elt]);
        }

        return node;
      }

    //--------------------------------------------

}
//==============================================
export class MainContent extends TreeControlItem {
//==============================================
    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<div>")
                    .addClass(this.data.classes);
        node.load(this.data.content);

        return node;
      }
    //--------------------------------------------
}
//==============================================
export class MainTitle extends TreeControlItem {
//==============================================
    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<h1>")
                    .text(this.data.content);

        return node;
      }
    //--------------------------------------------
}
//==============================================
export class TreeControl extends TreeControlItem {
//==============================================

    //--------------------------------------------
    onInit() {
    //--------------------------------------------
        this.data.current="/";
        this.title= new MainTitle(this);
        this.top= new MainArea(this,{'classes':'MainArea top','content':[]});
        this.bottom= new MainArea(this,{'classes':'MainArea bottom','content':[]});
        this.left= new MainArea(this,{'classes':'MainArea left','content':[]});
        this.right= new MainArea(this,{'classes':'MainArea right','content':[]});
        this.center= new MainContent(this,{'classes':'MainArea center','content':[]});

    }

    //---------------------------------------------------
    modify(selected) {
    //---------------------------------------------------
      this.data.current=selected;
      this.make();
      }

    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
      if(this.nodes){this.nodes.remove();}
      var dataset=this.getData(this.data.current);
      this.title.data.content=this.data.current;
      this.top.data.dataset=dataset["top"];
      this.bottom.data.dataset=dataset["bottom"];
      this.left.data.dataset=dataset["left"];
      this.right.data.dataset=dataset["right"];
      this.center.data.content=dataset["center"];

        var node=$("<div>")
                    .addClass("Main");

        return node;
      }


    //--------------------------------------------
    loadData(path,url) {
    //--------------------------------------------


      fetch(url)
        .then(function (response) {
          // console.log(url + " -> " + response.ok);
          if(response.ok){
            return response.text();
          }
          throw new Error('Error message.');
        })
        .then(function (data) {
          console.log("data: ", data);

        }.bind(this))
        .catch(function (err) {
          console.log("failed to load ", url, err.message);
        });
    }
    //--------------------------------------------
    getData(path){
    //--------------------------------------------
        console.log(path);
        var current_node=this.get_root().find(path);

        var data={};
        data["top"]=[];
        data["bottom"]=[];
        data["left"]=[];
        data["right"]=[];
        data["center"]="";

        if(current_node != current_node.get_root()){
            data["top"].push(get_node("root",current_node.get_root()));
        }
        if(current_node.parent && current_node.parent!= current_node.get_root()){
            data["top"].push(get_node("parent",current_node.parent));
        }
        data["top"].push(get_node("current",current_node));

        console.log(current_node.children);

        for (let child in current_node.children) {
            data["bottom"].push(get_node(current_node.children[child].data.name,current_node.children[child]));
        }

        data["center"] = "./test.html";


        console.log(data);

        return data;
    }
    //--------------------------------------------
}
//==============================================
