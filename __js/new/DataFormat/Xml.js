  import {TreeNode} from './../Tree/TreeNode.js';

  import {HtmlElement} from './../Html/HtmlElement.js';
  import {Form,Input,HiddenInput,Button,Select,SelectOption} from './../Html/Form.js';
  import {Accordion} from './../Html/Accordion.js';
  import {Tabs} from './../Html/Tabs.js';

  import {Scene} from './../3D/Scene.js';
  import {Camera} from './../3D/Camera.js';
  import {Object3D} from './../3D/Object3D.js';
  import {OrbitCamera} from './../3D/OrbitCamera.js';
  import {Sprite,SpriteGroup} from './../3D/Sprite.js';
  import {View360} from './../3D/View360.js';
  import {DemoCube} from './../3D/DemoCube.js';

  import {TreeControl} from './../TreeControl/TreeControl.js';

//======================================================
export class Xml extends TreeNode {
//======================================================



    //---------------------------------------------------
    onInit() {
    //---------------------------------------------------
        this.tree= null;

        if(this.data.url){

            if(this.data.method == "post"){

                $.post(this.data.url,this.data.request)
                        .done(this.onParse.bind(this))
                        .fail(function(){console.error("fail to load xml ",this.data.url);} );

            }else{
                $.get(this.data.url)
                        .done(this.onParse.bind(this))
                        .fail(function(){console.error("fail to load xml ",this.data.url);} );
            }
        }else if(this.data.data){
        console.log("loaded from string",this.data);
            this.read_data(this.data.content_id,this.data.data);

        }

     }

    //---------------------------------------------------
    onParse(data, textStatus, jqxhr){
    //---------------------------------------------------
        console.log(data, textStatus, jqxhr);
        console.log("loaded",this.data.url);
        var xml_data=$.parseXML(data);
        this.tree= this.onParseNode(xml_data.firstChild,null);

        if(this.data.content_id){
            this.tree.data.select=this.data.content_id;
        }
        //console.log(this.data);
        //console.log(this.tree);
        if(this.tree.make){
            console.log("make");
            this.tree.make();
        }

     }

    //---------------------------------------------------
    read_data(id,data){
    //---------------------------------------------------
        //console.log(data, textStatus, jqxhr);
        console.log("loaded",this.data.url);
        //console.log("loaded",data);
        var xml_data=$.parseXML(data);
        this.tree= this.onParseNode(xml_data.firstChild,null);

        this.tree.data.select=id;

        //if(this.tree.make){
            console.log(this.tree);
            this.tree.make();
       // }

     }
    //---------------------------------------------------
    onParseNode(xml_node,root){
    //---------------------------------------------------
        //console.log("XXXXX",xml_node.localName);
        //console.log(xml_node);

        var data={};
        for (let i = 0; i < xml_node.attributes.length; i++) {
            data[xml_node.attributes[i].name]=xml_node.attributes[i].value;
        } 
        var cls=eval(xml_node.localName);
        var node=new cls(root,data);
        //console.log(node);

        for (let i = 0; i < xml_node.children.length; i++) {
            this.onParseNode(xml_node.children[i],node);
        } 
        return node;
     }
    //---------------------------------------------------

}
//======================================================
