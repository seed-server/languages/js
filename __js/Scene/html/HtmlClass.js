
//========================================================

class HtmlClass extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("value");
        this.add_variable("remove",false);
        this.add_variable("toggle",false);
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        if(this.remove){
            div.removeClass(this.value);

        }else if(this.toggle){
            div.toggleClass(this.value);
        }else{    
            div.addClass(this.value);
        }
    }
    //----------------------------------------------------
}
//========================================================
