
//========================================================

class SceneGroup extends SceneNode {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);

        this.div=null;
        this.add_variable("part","body");
        this.add_variable("select",null);
    }

    //----------------------------------------------------
     onCall(data){
    //----------------------------------------------------
            var node=this.search_path("__main__");
            if(node){
                this.msg("DRAWSCENE",name,5);
                return node.draw(data.div,data.selected);
            }else{
                //console.log("draw not found",name,selected,div);
            }

        //return div;
    }
    //----------------------------------------------------
     onMake(data){
    //----------------------------------------------------
        var selected=null;

        if(this.select!=null){
            selected=this.search_path(this.select);
        }else{
            selected=this.root().content;
        }

        if(selected==null){
            //console.log("NO NODE SELECTED",this.path());
            return;
        }
        //console.log("SELECTED",selected.path());
        this.div=$(this.root().tag);
        //console.log(this.root().tag,$(this.root().tag));
        var div=null;
        if(this.div!=null){
           div=this.div;
        }else{
            div=$(this.part)[0];
        }
        this.drawScene("__main__",div,selected);
        //return div;
    }
    //----------------------------------------------------
    drawScene(name,div,selected){
    //----------------------------------------------------

        var node=this.search_path(name);
        if(node){
            //console.log("draw",name,selected,div);
            return node.draw(div,selected);
        }else{
            //console.log("draw not found",name,selected,div);
        }

    }
    //----------------------------------------------------

}
//========================================================



