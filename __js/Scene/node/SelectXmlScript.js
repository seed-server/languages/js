//========================================================

class SelectXmlScript extends SceneElement {

//========================================================
    //----------------------------------------------------
    draw(div,selected){
    //----------------------------------------------------


        this.LoadFile(this.get_url(selected),this.onFileLoaded.bind(this),"obj",null,{div:div,selected:selected});
    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------
        //console.log("onFileLoaded",data,data.selected)
        //data.response.attach(this.root());
        data.response.setup();
        data.response.call(data);//data.div,data.selected);
        //data.response.destroy()
    }
    //----------------------------------------------------

}
//========================================================
