//========================================================

class ForChildren extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("attr",null);
        this.add_variable("value",null);
        this.add_variable("cls",null);
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var lst=[];

      //console.log(this.path(),"################################",selected);
      if(this.cls!=null){
            var cls=eval(this.cls);

            lst=selected.children.by_class(cls);
        }else{
            lst=selected.children;
        }
      //console.log(this.path(),"################################",lst.size());
        if(this.attr!=null){
            lst=lst.by_attr_value(this.attr,this.value);
        }
        lst=lst.sorted();
        for(var i in lst) {

            super.draw(div,lst[i]);
        }
        return div;
    }
    //----------------------------------------------------

}
//========================================================

