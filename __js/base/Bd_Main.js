//========================================================

class Bd_Main extends Bd_Action {

//========================================================
    //----------------------------------------------------
    constructor(tag,command,content){
    //----------------------------------------------------

        super(null, {});

        this.tag=tag;
        this.content=content;
        this.command=command;
        this.baseurl=content.substring(0, content.lastIndexOf("/"));
        // site map
        new Query(this,content,this.onLoadContent.bind(this),"obj");
        new Query(this,command,this.onLoadCommand.bind(this),"obj");

        this.i=0;

    }

    onLoadContent(data){data.name="data";this.content=data;data.attach(this);this.onLoadFile(data);}
    onLoadCommand(data){data.name="__main__";this.command=data;data.attach(this);this.onLoadFile(data);}
    //----------------------------------------------------

    onLoadFile(data){

    //----------------------------------------------------
        this.i+=1;
        //data.attach(this);
        if(this.i==2){this.setup();}

    }
    //----------------------------------------------------

    onSetup(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.children.execute("setup");
        this.run();

    }
    //----------------------------------------------------

    run(){

    //----------------------------------------------------
        var node=this.search_path("__main__");
        if(this.command !=null){
            //console.log("make");
            this.command.tag=this.tag;
            this.command.call(); //{div:$("#"+this.tag)}
        }
    }
    //----------------------------------------------------

}
//========================================================

class Run extends Bd_Action {

//========================================================
    //----------------------------------------------------
    constructor(url,parent=null,callback=null){
    //----------------------------------------------------

        super(null, {});
        this.url=url;
        this.node=null;
        this.node_parent=parent;
        this.callback=this.run;
     
        // site map
        new Query(this,url,this.onLoadCommand.bind(this),"obj");
    }

    //----------------------------------------------------
    onLoadCommand(node){
    //----------------------------------------------------
        //data.name="__main__";
        this.node=node;
        //this.node.attach(this.node_parent);
        //data.attach(this);
        this.node.setup();
        //this.node.make();

        if(this.callback){
            this.callback(node);
        }
    }
    //----------------------------------------------------

    onSetup(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.children.execute("setup");
        this.run();  

    }
    //----------------------------------------------------

    run(){

    //----------------------------------------------------
        //this.node.print()

        var node=this.node.search_path("__main__");
        //if(node !=null){
        //    console.log("call");
        //    node.call();
        //}else{
         //   console.log("fail no __main__ to start");   
        //}
    }
    //----------------------------------------------------


}
//========================================================

