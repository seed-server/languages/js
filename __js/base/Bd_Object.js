
//========================================================

class Message extends Atom {

//========================================================
    //----------------------------------------------------
    constructor(parent,tag,data){
    //----------------------------------------------------
        super(parent,null);
        data["t"]=CLOCK.getTime();
        this.data=data;
        this.tag=tag;
    }
    //----------------------------------------------------
}
//========================================================

class Bd_Object extends Atom {

//========================================================
    // modules have lifecycle
    //  elt.setup()
    /* 
    objets serialisables
    */
    //----------------------------------------------------
    constructor(parent,data){
    //----------------------------------------------------
        if(data["name"]){
            super(parent,data["name"]);
        } else {
            super(parent,null);
        }
        this.__data=[];
        this.__setup_done=false;


        for (const key in data) {
            this.__data[key]=decodeURI(data[key]);
        }

    }
    //----------------------------------------------------
    add_variable(name,def=null){
    //----------------------------------------------------

        this[name]=def;
        if(this.__data[name] ) {
            this[name]=this.__data[name];
        }
    }
    //----------------------------------------------------
    setup(){
    //----------------------------------------------------

        if(this.__setup_done==false){

            this.msg("SETUP",{},2);
            this.onSetup(this.__data);
            this.ls_objects().execute("setup");
            this.__setup_done=true;
            this.onSetupDone();
        }
    }
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

    }
    //----------------------------------------------------
    onSetupDone(){
    //----------------------------------------------------


    }
    //----------------------------------------------------
    waitSetup(){
    //----------------------------------------------------


        if(this.isLoaded()==true){
            this.msg("SETUP_CHILDREN",{},2);
            this.objects().execute("setup");
            //this.onSetupDone();
            this.__setup_done=true;
            this.msg("SETUP_DONE",{},2);
        }else{
            this.msg("SETUP_WAIT",{},1);
            setTimeout(this.waitSetup.bind(this),DT);
        }
    }
    //----------------------------------------------------
    isLoaded(){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
    msgXX(tag,data){
    //----------------------------------------------------
        if ( DEBUG ) {
            //new Message(this,tag,data);
            //console.log(tag,this.path(),data);
        }

    }
    //----------------------------------------------------
    msgs(){
    //----------------------------------------------------
        return this.children.by_class(Message);
    }
    //----------------------------------------------------
    ls_objects(){
    //----------------------------------------------------
        return this.children.by_class(Bd_Object);
    }
    //----------------------------------------------------

}
//========================================================
